Welcome to Magestore Mentor's Checklist. 

We are happy to onboard you onto the Mentoring Journey where you and your mentee will go together during several months.


Below are our suggested checklist to help you shape an exciting journey with your mentee.

(*Notice that this's only our suggestion to complete in each stage. Besides, you can adjust the checklist or modify the template/exercise to suit with your mentoring relationship*) 

Refer to the Handbook the [Mentor's Guide](https://handbook.magestore.com/books/the-mentors-guide) for mass knowledge. You don't need to remember all the checklist, just copy it to your account and tick check along the way on your mentoring process.
Please contact MageX directly if you have concerns/ questions, or find something unreasonable. 

## Stage 0. Planning for mentoring
* [ ] Discover the mentoring concept - [Getting started chapter](https://handbook.magestore.com/books/the-mentors-guide/chapter/getting-started). 
* [ ] Explore [Key skills of a mentor](https://handbook.magestore.com/books/the-mentors-guide/chapter/key-mentoring-skills)
* [ ] Read overview about [stages of the relationship](https://handbook.magestore.com/books/the-mentors-guide/page/overview-about-key-mentoring-skills-f22)
* [ ] If possible, get some information about your mentee
* [ ] Reflect on your past mentoring experiences for insights to use in this new mentoring relationship. Complete and be ready to talk about [My Mentoring Experiences](https://handbook.magestore.com/books/the-mentors-guide/page/stage-0-planning-for-mentoring#bkmrk-exercise-1.-my-mento).
* [ ] Think about your [hopes & expectations for the mentoring relationship](https://handbook.magestore.com/link/342#bkmrk-exercise-2.-answer%3A-)
* [ ] Look over [My Personal Vision & Personal Vision Statement exercise](https://handbook.magestore.com/books/the-mentors-guide/page/stage-0-planning-for-mentoring#bkmrk-exercise-3.-crafting) (consider completing it). Be ready to talk with your mentee about potential development goals and activities.
* [ ] Prepare [Mentoring Agreement](https://handbook.magestore.com/books/the-mentors-guide/page/stages-1-exchanging-information-setting-goals#bkmrk-mentoring-agreement) (You will discuss with your mentee and adjust it) in Stage 1
* [ ] Think through what you’d like your mentoring relationship to do. Be ready to review and discuss your mentee’s [Goals for Mentoring Relationship](https://handbook.magestore.com/link/342#bkmrk-prepare-to-help-ment).
* [ ] Determine any limits you have (eg: are you travelling a lot over the next few months?
* [ ] Participating in mentor training activities.


## Stage 1. Building agreements & setting goals
* [ ] Review the materials you prepared ([My Mentoring Experiences](https://handbook.magestore.com/books/the-mentors-guide/page/stage-0-planning-for-mentoring#bkmrk-exercise-1.-my-mento), [Personal Vision](https://handbook.magestore.com/books/the-mentors-guide/page/stage-0-planning-for-mentoring#bkmrk-exercise-3.-crafting)) so you can ready to discuss each with your mentee.
* [ ] Meet with your mentee (in person or by video call) at ______________, (time, date, location).
* [ ] Use the [First Meeting Tool](https://handbook.magestore.com/books/the-mentors-guide/page/stages-1-building-agreement-setting-goals#bkmrk-1st-meeting-with-you), exchange personal information and why you’re participating in the program.
* [ ] Take time to listen and start building rapport. Find out some of your mentee’s background, interests, and reasons for agreeing to be a mentee.
* [ ] Review your mentee’s copies of My Mentoring Experiences, Personal Vision, tentative [Goals for Mentoring Program](https://handbook.magestore.com/books/the-mentors-guide/page/stage-0-planning-for-mentoring#bkmrk-prepare-to-help-ment). Discuss these, and make adjustments in goals if needed. Share what’s appropriate from your mentoring experiences and personal vision.
* [ ] Make [Mentoring agreements](https://handbook.magestore.com/books/the-mentors-guide/page/stages-1-building-agreement-setting-goals#bkmrk-mentoring-agreement) with your mentee
* [ ] Complete the rest of the First meeting tool.
* [ ] Schedule two or more future meetings with your mentee. These can be in person or by video call.
* [ ] Clarify what you agree to do before the next meeting



## Stage 2. Working towards goals/Deepening the Engagement
* [ ] Help your mentee prepare an [Action Plan](https://handbook.magestore.com/books/the-mentors-guide/page/stage-2-working-towards-goalsdeepening-the-engagement#bkmrk-action-plan) towards their goals.
* [ ] Say thanks for specific help or consideration given to you.
* [ ] Compliment mentee on his/her skills, knowledge, and attitudes as well as other things you observe
* [ ] Take him/her to breakfast, lunch or dinner
* [ ] Pass on information that could be useful to your mentee.
* [ ] Continue to meet (in person, on the phone, plus email) on a regular basis.
* [ ] Monitor and make changes, as appropriate, in mentee’s action plan
* [ ] If appropriate, recommend people for your mentee, and perhaps interview.Suggest clues on how to interact with these individuals. Ask your mentee to let you know how these encounters turn out.
* [ ] Offer resources (e.g: books, articles, podcasts, videos, …) you believe would be helpful, or inspiring. Suggest that your mentee look for resources on her/his own.
* [ ] If appropriate, attend key meetings (of you of his/her) with your mentees
* [ ] Determine both of your roles beforehand.
* [ ] After that, debrief the meetings with him/her, explain your view of what was happening. Discuss the dynamics of the meetings.
* [ ] Follow through promptly on every commitment you make to your mentee (if you are delayed, let him/her know why)
* [ ] As appropriate, provide some instruction
* [ ] Share aspects of your career story, including how you made key decisions
* [ ] Teach specific techniques you have found useful in work with clients, customers, colleagues, and others.
* [ ] If appropriate, ask your mentee to observe you performing the skills he/she is developing
* [ ] Continue to give corrective feedback on your mentee’s ideas and performance
* [ ] Continue to answer questions about your organization’s policies, procedures, culture, and politics
* [ ] Offer coaching on a specific performance or actions your mentee must take. Provide feedback on the guideline/program to MageX
* [ ] Halfway through your mentoring relationship, do an evaluation of your progress and the effectiveness of your relationship.
* [ ] Complete the [Midway Review](https://handbook.magestore.com/books/the-mentors-guide/page/stage-2-working-towards-goalsdeepening-the-engagement#bkmrk-midway-review) and discuss it with your mentee. If requested, give a copy to MageX.
* [ ] With your mentee, discuss what you both concluded from the review. Based on the discussion, renegotiate any aspects of the mentoring relationship.
* [ ] Review the status of your mentee’s goals and development activities
* [ ] Together, adjust former goals or add new ones and note new development activities
* [ ] Negotiate a timeline, schedule, etc.
* [ ] Make modifications in the relationship as needed


## Stage 3. Ending the mentoring relationship & planning for the future
* [ ] About a month before you’re ready to end the mentoring relationship, reflect on what your mentee has accomplished and what you have gained.
* [ ] Complete the [Final Review](https://handbook.magestore.com/books/the-mentors-guide/page/stage-3-ending-the-mentoring-relationship-planning-for-the-future#bkmrk-final-review) and give to MageX
* [ ] Complete the [Mentoring Program Evaluation](https://handbook.magestore.com/books/the-mentors-guide/page/stage-3-ending-the-mentoring-relationship-planning-for-the-future#bkmrk-mentoring-program-ev) and give to MageX
* [ ] Meet in person or via video call with your mentee to discuss the status of her/his development activities & goals, your relationship and any “unfinished business”.
* [ ] Follow-up your closure meeting with a note of thanks
* [ ] Express appreciation to the ones who helped you in the mentoring journey, if appropriate
* [ ] As appropriate, sometimes touch base with your mentee in the future
* [ ] If appropriate, find another mentoring relationship, either as a mentee or as a mentor

