Welcome to Magestore Mentee's Checklist.

We are happy to onboard you onto the Mentoring Journey where you and your mentor will go together during several months.
Below are our suggested checklist to help you shape an exciting journey with your mentor. (*It's only our suggestion to complete in each stage. Besides, you can adjust the checklist or modify the template/exercise to suit with your mentoring relationship*) )

Refer to the Handbook the [Mentee's Guide](https://handbook.magestore.com/books/the-mentees-guide) for mass knowledge. You don't need to remember all the checklist, just copy it to your account and tick check along the way on your mentoring process.
Please contact MageX directly if you have concerns/ questions, or find something unreasonable.

## Stage 0. Planning for mentoring
* [ ] Discover the mentoring concept - [Getting started chapter](https://handbook.magestore.com/books/the-mentees-guide/chapter/getting-started).
* [ ] Explore [Key skills of a mentee](https://handbook.magestore.com/books/the-mentees-guide/chapter/key-skills-for-mentees)
* [ ] Read overview about [Stages of the mentoring relationship](https://handbook.magestore.com/books/the-mentees-guide/chapter/stages-of-formal-mentoring-relationship)
* [ ] Reflect on your past mentoring experiences for insights to use in this new mentoring relationship. Complete and be ready to talk about [My Mentoring Experiences](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-my-mentoring-experie)
* [ ] If possible, get some information about your mentor.
* [ ] Complete [My Personal Vision & Personal Vision Statement exercise](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-my-personal-vision).Think about your potential development goals & activities and be ready to share all these with your mentor.
* [ ] Prepare [Mentoring Agreement](https://handbook.magestore.com/books/the-mentees-guide/page/stages-1-building-agreements-setting-goals#bkmrk-mentoring-agreement) (You will discuss with your mentor and adjust it and/or give a copy to MageX (as the coordinator of the mentoring program)
* [ ] Think through what you’d like your mentoring relationship to do. Draft [Goals for Mentoring Relationship](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-setting-goals-for-yo) to discuss with your mentor.
* [ ] Determine any limits you have (eg: are you travelling a lot over the next few months? Are you caring for an ill parent?)
* [ ] Participating in mentee training activities.

## Stage 1. Building agreements & setting goals
* [ ] Review the materials you prepared ([My Mentoring Experiences](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-my-mentoring-experie), [Personal Vision](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-my-personal-vision)) so you can ready to discuss each with your mentor.
* [ ] Meet with your mentor (in person or by video call) at ______________, (time, date, location).
* [ ] Use the [First Meeting Tool](https://handbook.magestore.com/books/the-mentees-guide/page/stages-1-building-agreements-setting-goals#bkmrk-first-meeting-tool), exchange personal information and why you’re participating in the program.
* [ ] Take time to listen and start building rapport. Find out some of your mentor’s background, interests, and reasons for agreeing to be a mentor.
* [ ] Show your mentor your copies of My Mentoring Experiences, Personal Vision and tentative [Goals for Mentoring Relationship](https://handbook.magestore.com/books/the-mentees-guide/page/stage-0-planning-for-mentoring-relationships#bkmrk-setting-goals-for-yo). Discuss these, and make adjustments in goals if needed.
* [ ] Make [Mentoring agreements](https://handbook.magestore.com/books/the-mentees-guide/page/stages-1-building-agreements-setting-goals#bkmrk-mentoring-agreement) with your mentor
* [ ] Complete the rest of the First meeting tool
* [ ] Schedule two or more future meetings with your mentor. These can be in person or by video call.
* [ ] Clarify what you agree to do before the next meeting


## Stage 2. Working towards goals/Deepening the Engagement
* [ ] Prepare an [Action Plan](https://handbook.magestore.com/link/379#bkmrk-action-plan) towards your goals.
* [ ] Say thanks for specific help of your mentor.
* [ ] Compliment mentor on his/her skills, knowledge, and attitudes as well as other things you observe.
* [ ] Take him/her to breakfast, lunch or dinner
* [ ] Pass on information that could be useful to your mentor..
* [ ] Continue to meet (in person, on the phone, plus email) on a regular basis.
* [ ] Monitor and make changes, as appropriate, in your Action plan
* [ ] If mentor offers, meet people he/she recommends.Ask for clues on how to interact with these individuals. Let your mentor know how these encounters turn out.
* [ ] Study and be prepared to discuss resources (eg: books, articles, videos,...) given or loaned to you by your mentor.
* [ ] If appropriate, attend key meetings (of you of his/her) with your mentor.
* [ ] Determine both of your roles in the key meetings beforehand
* [ ] After that, debrief the meetings with him/her, explain your view of what was happening. (If not offered, ask for your mentor’s input on the dynamics of the meeting).
* [ ] Follow through promptly on every commitment you make to your mentor (if you are delayed, let him/her know why)
* [ ] Continue to learn directly from your mentor.
* [ ] Interview your mentor on aspects of your career story, including how key decisions were.
* [ ] Ask for specific techniques your mentor has used to work with clients, customers, colleagues, and others.
* [ ] If appropriate, observe your mentor performing the skills you're developing
* [ ] Continue to ask for corrective feedback on your ideas and performance
* [ ] Continue to ask specific questions about your organization’s policies, procedures, culture, and politics
* [ ] Ask for coaching on a specific performance or actions you must take, for example, as a presentation you’re giving
* [ ] Provide feedback on the guideline/program to MageX
* [ ] Halfway through your mentoring relationship, do an evaluation of your progress and the effectiveness of your relationship.
* [ ] Complete the [Midway Review](https://handbook.magestore.com/link/379#bkmrk-midway-review) and discuss it with your mentor. If requested, give a copy to MageX
* [ ] With your mentor, discuss what you both concluded from the review
* [ ] Based on the discussion, renegotiate any aspects of the mentoring relationship
* [ ] Review the status of your goals and development activities.
* [ ] Together, adjust former goals or add new ones and note new development activities
* [ ] Negotiate a timeline, schedule, etc.
* [ ] Make modifications in the relationship as needed

## Stage 3. Ending the mentoring relationship & planning for the future

* [ ] About a month before you’re ready to end the mentoring relationship, reflect on what you have accomplished and what you have gained.
* [ ] Complete the [Final Review](https://handbook.magestore.com/link/409#bkmrk-final-review) and give to MageX (as a coordinator of mentoring program at Magestore)
* [ ] Complete the [Mentoring Program Evaluation](https://handbook.magestore.com/link/409#bkmrk-mentoring-program-ev) and give to MageX
* [ ] Meet in person or via video call with your mentor to discuss the status of your development activities & goals, your relationship and any “unfinished business”
* [ ] Follow-up your closure meeting with a note of thanks
* [ ] Express appreciation to the ones who helped you in the mentoring journey, if appropriate
* [ ] As appropriate, sometimes touch base with your mentor in the future
* [ ] If appropriate, find another mentoring relationship, either as a mentee or as a mentor
